# README #

Submission for Alexathon - https://www.hackerearth.com/sprints/alexa-hackathon/  
Demo Video - https://youtu.be/U3CTXLRpP0Y  
Sample session - https://youtu.be/aI80KlH_hhU  
 
### What is this repository for? ###

This is the source code deployed on AWS Lambda for the Alexa Skill - Ghost Game

### How do I get set up? ###

Deploy on AWS lambda and set-up the Alexa Skill
The word list is a trie made in python and dumped using pickle, so that dictionary load is fast (very fast)

### Who do I talk to? ###

Visit me at [https://virresh.github.io/](https://virresh.github.io/)