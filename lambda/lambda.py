import random
import sys
import pickle
import string

wordTrie = {}
curWord = None
curLetter = None
intentType = None
err = 0

response = {
  "version": "1.0",
  "sessionAttributes": {
    "word":None
  },
  "response": {
    "outputSpeech": {
      "type": "SSML",
      "ssml": "<speak>Welcome to the Ghost Game ! Choose an Alphabet to Start.</speak>"
    },
    "shouldEndSession": False
  }
}

def loadDictionary():
    with open("wordPickeDump.txt","rb") as pf:
        global wordTrie
        wordTrie = pickle.load(pf)

def isValidWord(word):
    if not word:
        return False
    ptr = wordTrie
    for i in range(0,len(word)):
        char = word[i]
        if(char in ptr):
            ptr = ptr[char]
        else:
            return False
    if(None in ptr):
        return True
    return False

def predictNextChar(word):
    ptr = wordTrie
    for char in word:
        if(char in ptr):
            ptr=ptr[char]
        else:
            return None
    for w in list(ptr):
        if( w!= None):
            return w
    return None

def parseE(event):
    global intentType 
    global curLetter
    global curWord
    global response
    global err
    intentType = event["request"]["type"]
    err = 0
    if "session" in event and "attributes" in event["session"] and "word" in event["session"]["attributes"]:
        curWord = event["session"]["attributes"]["word"]

    if(intentType == "LaunchRequest"):
        response["response"]["shouldEndSession"] = False
        response["sessionAttributes"]["word"] = curWord = None
        curLetter = None
        print("New Session Detected !")

    elif(intentType == "IntentRequest" and event["request"]["intent"]["name"] == "LaunchGame"):
        alphaSlot = event["request"]["intent"]["slots"]["Alphabets"]
        status = alphaSlot["resolutions"]["resolutionsPerAuthority"][0]
        if(status["status"]["code"] == "ER_SUCCESS_MATCH"):
            curLetter = status["values"][0]["value"]["name"].lower()
        else:
            err = 1
        curWord= event["session"]["attributes"]["word"]
        print("Existing Session - W= " + str(curWord) + " L= " + str(curLetter))

    elif(intentType == "IntentRequest" and event["request"]["intent"]["name"] == "ChallengeIntent"):
        err = 5
        curWord = event["session"]["attributes"]["word"]
        response["response"]["shouldEndSession"] = True
        if(curWord == None):
            response["response"]["shouldEndSession"] = False
            response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                            "<say-as interpret-as='interjection'>{}</say-as>, "
                                                            "There isn't any word yet. "
                                                            "Start by saying <emphasis>letter a</emphasis> "
                                                            "</speak>").format("oye")
        else:
            if(isValidWord(curWord)):
                response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                                "<say-as interpret-as='interjection'>{}</say-as>, "
                                                                "You caught me ! "
                                                                "<emphasis>Congratulations</emphasis> on your win ! "
                                                                "If you enjoyed it please show your support to the developer "
                                                                "by rating the game on the store. "
                                                                "See you next time ! "
                                                                "</speak>").format("shabash")
            else:
                ch = predictNextChar(curWord)
                if(ch == None):
                    response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                                    "<say-as interpret-as='interjection'>{}</say-as>, "
                                                                    "You caught me ! "
                                                                    "<emphasis>Congratulations</emphasis> on your win ! "
                                                                    "If you enjoyed it please show your support to the developer "
                                                                    "by rating the game on the store. "
                                                                    "See you next time ! "
                                                                    "</speak>").format("shabash")
                else:
                    tempW = curWord
                    while(ch != None):
                        tempW = tempW + ch
                        ch = predictNextChar(tempW)

                    response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                                    "<say-as interpret-as='interjection'>{}</say-as>, "
                                                                    "One possible word is {}, spelled as "
                                                                    "<say-as interpret-as='spell-out'>{}</say-as> . "
                                                                    "You lost ! Better Luck next time ! "
                                                                    "If you enjoyed it please show your support to the developer "
                                                                    "by rating the game on the store. "
                                                                    "See you next time ! "
                                                                    "</speak>").format("nah",tempW, tempW)
                

        print("Challenging from user")

    elif(intentType == "IntentRequest" and (event["request"]["intent"]["name"] == "AMAZON.StopIntent")):
        err = 2
        response["sessionAttributes"]["word"] = None
        response["response"]["shouldEndSession"] = True
        response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                        "We shall play some other time then. "
                                                        "See ya."
                                                        "</speak>")
        print("Stop Intent Recieved")

    elif(intentType == "IntentRequest" and (event["request"]["intent"]["name"] == "AMAZON.CancelIntent")):
        err = 3
        curWord= event["session"]["attributes"]["word"]
        if(len(curWord) >= 2):
            response["sessionAttributes"]["word"] = curWord[:-2]
            curWord = curWord[:-2]
            response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                            "Okay, "
                                                            "We are now back to {} ."
                                                            "</speak>").format(curWord)
        else:
            response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                            "<say-as interpret-as='interjection'>{}</say-as>, "
                                                            "Rollback is not possible ! "
                                                            "</speak>").format("oye")
            
        response["response"]["shouldEndSession"] = False
        print("Cancel Intent to " + response["sessionAttributes"]["word"] )

    elif(intentType == "IntentRequest" and event["request"]["intent"]["name"] == "AMAZON.HelpIntent"):
        err = 4
        if("word" in event["session"]["attributes"]):
            response["sessionAttributes"]["word"] = event["session"]["attributes"]["word"]
        response["response"]["shouldEndSession"] = False
        response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                        "Try to give the next alphabet "
                                                        "such that you don't make a valid word yet, "
                                                        "but construction of one is still possible. "
                                                        "Just say a letter, like letter A to add your letter. "
                                                        "If you want to quit, say <emphasis>stop this game</emphasis> . "
                                                        "If you want to challenge, say <emphasis>I challenge you</emphasis> . "
                                                        "If you spoke a letter by mistake, say <emphasis>cancel</emphasis> . "
                                                        "</speak>")
        print("Help Intent Recieved !")

def buildResponse():
    global curLetter
    global curWord
    global wordTrie
    global err
    global intentType
    global response

    response["response"]["outputSpeech"]["type"] = "SSML"   # set to use SSML

    if (err == 2 or err == 3 or err == 4 or err == 5):
        # ending conditions, the speech variables have already been set while parsing event
        response["sessionAttributes"]["word"] = curWord 
        return

    if ((intentType != "LaunchRequest") and ((err == 1) or (curLetter==None) or (curWord == None and curLetter == None) or (curWord == None and curLetter not in string.ascii_lowercase))):
        # non-identifiable character
        response["response"]["shouldEndSession"] = False
        response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                        "<say-as interpret-as='interjection'>{}</say-as>, "
                                                        "The letter was not recognised. "
                                                        "Please Choose an alphabet. "
                                                        "If you don't want to play anymore, say <emphasis>Stop this game</emphasis> . "
                                                        "If you spoke a letter by mistake, say <emphasis>cancel</emphasis> . "
                                                        "</speak>").format("aiyo")
        response["sessionAttributes"]["word"] = curWord 
        return

    if (intentType == "LaunchRequest"):
        # start a new Game
        num = random.randint(0,10)
        if(num < 5):
            response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                            "Welcome to the Ghost Game. Start with a letter. "
                                                            "For example, say <emphasis>letter A</emphasis>. "
                                                            "</speak>")
            response["sessionAttributes"]["word"] = None
            response["response"]["shouldEndSession"] = False
        else:
            i = random.randint(0,len(wordTrie))
            response["sessionAttributes"]["word"] = list(wordTrie)[i]
            w = response["sessionAttributes"]["word"]
            response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                            "I start with {}. "
                                                            "Choose the next letter."
                                                            "</speak>").format(w)
            response["response"]["shouldEndSession"] = False
        return


    if curWord == None:
        curWord = curLetter
    else:
        curWord = curWord + curLetter

    response["sessionAttributes"]["word"] = curWord            

    if(isValidWord(curWord)):
        # user has made a valid word, they lost !
        response["response"]["shouldEndSession"] = True
        response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                        "<say-as interpret-as='interjection'>{}</say-as>, "
                                                        "You lost by making a completely valid word, {}. "
                                                        "If you enjoyed it please show your support to the developer "
                                                        "by rating the game on the store. "
                                                        "See you next time !"
                                                        "</speak>").format("ha",curWord)
        return

    c = predictNextChar(curWord)

    k = random.randint(0,100)
    if(k>=90):
        # Bluff with a 10% chance !
        i = random.randint(0,len(wordTrie))
        c = list(wordTrie)[i]

    if (c == None):
        # no valid word can be made from curWord, if curWord is already a valid word, user would have lost above
        response["response"]["shouldEndSession"] = True
        response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                        "<say-as interpret-as='interjection'>{}</say-as>, "
                                                        "No word can be made from, {}. You lose !"
                                                        "If you enjoyed it please show your support to the developer "
                                                        "by rating the game on the store. "
                                                        "See you next time !"
                                                        "</speak>").format("uh huh",curWord)
        return

    else:
        # add the next character and play
        response["sessionAttributes"]["word"] = curWord + c
        response["response"]["shouldEndSession"] = False
        response["response"]["outputSpeech"]["ssml"] = ("<speak>"
                                                        "<emphasis>{}</emphasis>, "
                                                        "I choose the letter <emphasis>{}</emphasis>. "
                                                        "The word now is "
                                                        "<say-as interpret-as='spell-out'>{}</say-as> . "
                                                        "Your turn to choose an alphabet."
                                                        "</speak>").format("Okay",c,curWord+c)
        return


def lambda_handler(event, context):
    global curLetter
    global curWord
    global wordTrie
    global err
    global intentType
    global response
    
    if not wordTrie:
        loadDictionary()
        print("DictionaryLoaded")
    parseE(event)
    print(err)
    buildResponse()
    # response["sessionAttributes"]["word"] = curWord 
    
    return response

if __name__ == '__main__':
    loadDictionary()
    print(isValidWord(sys.argv[1]))
    

    # if(err == 2 or err == 3 or err == 4):
    #     pass
    # elif(intentType == "LaunchRequest"):
    #     num = random.randint(0,10)
    #     if(num < 5):
    #         print("Give user the first Chance.")
    #         response["response"]["outputSpeech"]["text"] = "Welcome to the Ghost Game. Start with a letter. For example, say letter A."
    #     else:
    #         i = random.randint(0,len(wordTrie))
    #         response["sessionAttributes"]["word"] = list(wordTrie)[i];
    #         response["response"]["outputSpeech"]["text"] = "I start with " + response["sessionAttributes"]["word"] + ". Choose the next letter."
    #         print("Start with " + response["sessionAttributes"]["word"])
    # elif (err == 1):
    #     response["response"]["outputSpeech"]["text"] = "Uh, the letter was not recognised. Please Choose an alpahbet."
    # else:
    #     if (curWord == None):
    #         curWord = curLetter
    #     else:
    #         curWord = curWord + curLetter
    #     response["sessionAttributes"]["word"] = curWord
    #     c = predictNextChar(curWord)
    #     if (c == None):
    #         if((not isValidWord(curWord))):
    #             response["response"]["outputSpeech"]["text"] = "I think you are bluffing. If you have a word in mind, speak it out, I will admit my defeat. Else you lose."
    #         else:
    #             response["response"]["outputSpeech"]["text"] = "Congratulations, you win !!! I cannot add anything to " + curWord
    #             response["response"]["shouldEndSession"] = True
    #     else:
    #         curWord = curWord + c
    #         response["sessionAttributes"]["word"] = curWord
    #         response["response"]["outputSpeech"]["text"] = "I choose " + c +". The word now is " + curWord +". Your turn to choose an alphabet."